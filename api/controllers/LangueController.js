/**
 * LangueController
 *
 * @description :: Server-side logic for managing langues
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	fr: (req, res) => {
		if (req.session.passport) {
			User.findOne({ id: req.session.passport.user }, (err, user) => {
				if (err) return done(err);
				if (!user){
					req.session.languagePreference = 'fr';
					return res.redirect('/hypertube');
				}
				user.langue = 'fr';
				user.save((err) => {
					if (err) console.error('ERROR!');
					req.session.languagePreference = 'fr';
					return res.redirect('/hypertube');
				});
			});
		} else {
			req.session.languagePreference = 'fr';
			return res.redirect('/hypertube');
		}
	},

	en: (req, res) => {
		if (req.session.passport) {
			User.findOne({ id: req.session.passport.user }, (err, user) => {
				if (err) return done(err);
				if (!user){
					req.session.languagePreference = 'en';
					return res.redirect('/hypertube');
				}
				user.langue = 'en';
				user.save((err) => {
					if (err) console.error('ERROR!');
					req.session.languagePreference = 'en';
					return res.redirect('/hypertube');
				});
			});
		} else {
			req.session.languagePreference = 'en';
			return res.redirect('/hypertube');
		}
	},

	es: (req, res) => {
		if (req.session.passport) {
			User.findOne({ id: req.session.passport.user }, (err, user) => {
				if (err) return done(err);
				if (!user){
					req.session.languagePreference = 'es';
					return res.redirect('/hypertube');
				}
				user.langue = 'es';
				user.save((err) => {
					if (err) console.error('ERROR!');
					req.session.languagePreference = 'es';
					return res.redirect('/hypertube');
				});
			});
		} else {
			req.session.languagePreference = 'es';
			return res.redirect('/hypertube');
		}
	},

	it: (req, res) => {
		if (req.session.passport) {
			User.findOne({ id: req.session.passport.user }, (err, user) => {
				if (err) return done(err);
				if (!user){
					req.session.languagePreference = 'it';
					return res.redirect('/hypertube');
				}
				user.langue = 'it';
				user.save((err) => {
					if (err) console.error('ERROR!');
					req.session.languagePreference = 'it';
					return res.redirect('/hypertube');
				});
			});
		} else {
			req.session.languagePreference = 'it';
			return res.redirect('/hypertube');
		}
	},

	de: (req, res) => {
		if (req.session.passport) {
			User.findOne({ id: req.session.passport.user }, (err, user) => {
				if (err) return done(err);
				if (!user){
					req.session.languagePreference = 'de';
					return res.redirect('/hypertube');
				}
				user.langue = 'de';
				user.save((err) => {
					if (err) console.error('ERROR!');
					req.session.languagePreference = 'de';
					return res.redirect('/hypertube');
				});
			});
		} else {
			req.session.languagePreference = 'de';
			return res.redirect('/hypertube');
		}
	}
};
