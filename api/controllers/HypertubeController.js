import request from 'request'
/**
 * HypertubeController
 *
 * @description :: Server-side logic for managing hypertubes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	hypertube: (req, res) => {
		// Call DB movie
		request (`${req.protocol}://${req.get ('host')}/api/v1/getActors`, (err, response, body) => {
			User.findOne({ id: req.session.passport.user }, (err, user) => {
				if (err) return done(err);
				if (!user) return res.redirect('/');
				const movieView = user.movieView;
				res.view('hypertube', {
					ret: body,
					movieView,
				});
			});
		})
	},
};
