import mime from 'mime';
import fs from 'fs';
import download from '../services/download';
import streamVideo from '../services/stream';

const Entities = require('html-entities').XmlEntities;
/**
* StreamController
*
* @description :: Server-side logic for managing streams
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {

	comment: (req, res) => {
		User.findOne({ id: req.session.passport.user }, (err, user) => {
			if (err || user === null || user === undefined) return res.send({ message: 'Error with user' });
			Movie.findOne(req.params.id, (err, movie) => {
				if (err || movie === null || movie === undefined) return res.send({ message: 'Error with movie' });
				if (!movie.commentaires) movie.commentaires = [];
				if (req.body.comment !== '') {
					const entities = new Entities();
					const comment = entities.encodeNonASCII(req.body.comment);
					if (user.image.indexOf('images/') !== -1)
						user.image = '/' + user.image;
					const tab = [req.session.passport.user, user.username, user.image, comment];
					movie.commentaires.push(tab);
					movie.save((err) => {
						if (err) return res.send({ message: 'Error with movie' });
						return res.send({ comment: tab });
					})
				} else {
					return res.send({ error: 'empty comment' });
				}
			});
		});
	},

	download: (req, res) => {
		Movie.findOne(req.params.id, (err, movie) => {
			if (err || movie === null || movie === undefined) return console.log('Erreur download movie');
			else {
				let stream;
				movie.torrents.forEach((resolution) => {
					if (resolution.quality === req.params.quality) stream = resolution;
				});
				if (stream.path && stream.path !== ''){
					const path = './movie/' + req.params.id + '/' + stream.quality + '/' + stream.path;
					const mimetype = mime.lookup(path);
					res.setHeader('Content-disposition', 'attachment; filename=' + path);
					res.setHeader('Content-type', mimetype);
					var filestream = fs.createReadStream(path);
					filestream.pipe(res);
				}
			}
		});
	},

	watch: (req, res) => {
		const id = req.params.id;
		const quality = req.params.quality;
		User.findOne({ id: req.session.passport.user }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.redirect('/');
			if (!user.movieView) user.movieView = [id];
			else if (user.movieView.indexOf(id) === -1) user.movieView.push(id);
			user.save((err) => {
				if (err) return
				// console.log('Start watching '.cyan);
				Movie.findOne(id, (err, movie) => {
					if (err || movie === null || movie === undefined) return res.redirect('hypertube');
					movie.lastView = new Date;
					movie.save((err) => {
						if (err) console.error('ERROR!');
						let barre = 0;
						movie.torrents.forEach((resolution) => {
							if (resolution.quality === quality){
								if (resolution.path && resolution.path !== '') barre = 1;
							}
						});
						res.view('stream', { movie, quality, id, barre, user });
					});
				});
			});
		});
	},

	stream: (req, res) => {
		const id = req.params.id;
		const quality = req.params.quality;
		Movie.findOne(id, (err, movie) => {
			if (err || movie === null || movie === undefined) return false;
			// console.log('Stream -> '.cyan + movie.title);
			let currentTorrent
			let stream;
			movie.torrents.forEach((resolution) => {
				if (resolution.quality === quality) {
					stream = resolution;
					currentTorrent = resolution
				}
			})
			if (stream === undefined) return false;
			const path = './movie/' + id + '/' + quality;

			if (fs.existsSync (path + '/' + currentTorrent.tmpPath) && !currentTorrent.isDownloading && currentTorrent.percent >= 100) {
				streamVideo(res, req, path + '/' + (currentTorrent.tmpPath ? currentTorrent.tmpPath : currentTorrent.path));
				return true;
			}

			else if (fs.existsSync (path + '/' + currentTorrent.tmpPath) && currentTorrent.isDownloading && currentTorrent.percent >= 30) {
				streamVideo(res, req, path + '/' + (currentTorrent.tmpPath ? currentTorrent.tmpPath : currentTorrent.path));
				return true;
			}

			// /mp4$/.test (currentTorrent.tmpPath) && fs.existsSync (path + '/' + currentTorrent.tmpPath) &&
			else if (currentTorrent.isDownloading) {
				res.status(200);
				return true;
			}

			else {
				download(res, req, id, quality, movie, stream, path).then(
					(data) => {
						// console.log (`[Trying to stream ${`${path}/${data.path}`} from data]`.blue)
						streamVideo(res, req, path + '/' + data.path);
					},
					(err) => {
						return false;
					}
				);
			}
		})
	},
};
