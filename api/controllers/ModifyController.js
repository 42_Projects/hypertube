import bcrypt from 'bcrypt';

/**
* ModifyController
*
* @description :: Server-side logic for managing modifies
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {

	modifyPassword: (req, res) => {
		if (req.body.confirmnewpassword !== req.body.newpassword)
		return res.send({ error: 'Password does not match'});
		User.findOne({ id: req.session.passport.user }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.send({ error: 'User not found' });
			if (user.omniauth === true)
			return res.send({ error: 'Can\'t change email' });
			user.password = req.body.confirmnewpassword;
			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(user.password, salt, (error, hash) => {
					if (error) return res.send({ message: 'Erreur' })
					user.password = hash;
					user.save((err) => {
						if (err) console.error('ERROR!');
						return res.send({ success: 'Your password has been changed' });
					});
				});
			});
		});
	},

	modifyEmail: (req, res) => {
		const regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
		if (!req.body.email || !regex.test(req.body.email)) {
			return res.send({ error: 'Email invalid' });
		}
		User.findOne({ email: req.body.email }, (err, user) => {
			if (err) return done(err);
			if (!user){
				User.findOne({ id: req.session.passport.user }, (err, user) => {
					if (err)
						return done(err);
					if (!user)
						return res.send({ error: 'User not found' });
					if (user.omniauth === true)
						return res.send({ error: 'Can\'t change email' });
					user.email = req.body.email;
					user.save((err) => {
						if (err) console.log('ERROR upload photo');
						return res.send({ success: 'Email updated' });
					});
				});
			}
			else {
				return res.send({ error: 'Email already exist' });
			}
		});
	},

	modifyUsername: (req, res) => {
		if (!req.body.username || req.body.username.length < 5) {
			return res.send({ error: 'Username is short' });
		}
		User.findOne({ username: req.body.username }, (err, user) => {
			if (err) return done(err);
			if (!user){
				User.findOne({ id: req.session.passport.user }, (err, user) => {
					if (err) return done(err);
					if (!user) return res.send({ error: 'User not found' });
					user.username = req.body.username;
					user.save((err) => {
						if (err) console.log('ERROR upload photo');
						return res.send({ success: 'username updated' });
					});
				});
			}
			else {
				return res.send({ error: 'Username already exist' });
			}
		});
	},

	modifyLastName: (req, res) => {
		if (!req.body.lastName || req.body.lastName.length < 2) {
			return res.send({ error: 'LastName is too short' });
		}
		User.findOne({ id: req.session.passport.user }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.send({ error: 'User not found' });
			user.lastName = req.body.lastName;
			user.save((err) => {
				if (err) console.log('ERROR upload photo');
				return res.send({ success: 'Last name updated' });
			});
		});
	},

	modifyFirstName: (req, res) => {
		if (!req.body.firstName || req.body.firstName.length < 2) {
			return res.send({ error: 'FirstName is too short' });
		}
		User.findOne({ id: req.session.passport.user }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.send({ user: 'User not found' });
			user.firstName = req.body.firstName;
			user.save((err) => {
				if (err) console.log('ERROR upload photo');
				return res.send({ success: 'First name updated' });
			});
		});
	},
};
