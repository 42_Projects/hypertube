import fs from 'fs';
import bcrypt from 'bcrypt';
import randomstring from 'randomstring';
import * as test from '../services/42api';
import authController from './AuthController';
import mailer from '../services/sendMail';
import mime from 'mime-magic';

/**
* UserController
*
* @description :: Server-side logic for managing users
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {
	create: (req, res) => {
		if (!req.body)
		return res.send({ message: 'Username is short' });
		if (!req.body.username || req.body.username.length < 5) {
			return res.send({ message: 'Username is short' });
		}
		if (!req.body.lastName || req.body.lastName < 2 || !req.body.firstName || req.body.firstName < 2) {
			return res.send({ message: 'lastName or firstNam is short' });
		}
		const regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
		if (!req.body.email || !regex.test(req.body.email)) {
			return res.send({ message: 'Email invalid' });
		}
		const regexPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
		if (!req.body.password || !regexPassword.test(req.body.password)) {
			return res.send({ message: 'Invalid password - You need numbers and letters' });
		}
		User.findOne({ username: req.body.username }, (err, user) => {
			if (err) return done(err);
			if (user) return res.redirect('/');
			else {
				User.create({
					username: req.body.username,
					lastName: req.body.lastName,
					firstName: req.body.firstName,
					email: req.body.email,
					image: 'images/avatar/default.jpg',
					password: req.body.password,
					langue: 'en',
					omniauth: false,
				}).exec((err) => {
					if (err) {
						return res.send({
							message: err,
						});
					}
					authController.login(req, res);
				})
			}
		});

	},

	upload: (req, res) => {
		res.view('upload');
	},

	userUpload: (req, res) => {
		req.file('file').upload(
			{
				maxBytes: 10 * 1000 * 1000, // 10 Mo
				dirname: '../../assets/images/avatar/'
			}, (err, uploadedFiles) => {
				if (err) return res.send({message: 'No file uploaded'});
				if (!uploadedFiles[0]) return res.send({message: 'No file uploaded'});
				if (uploadedFiles[0].type !== 'image/png' && uploadedFiles[0].type !== 'image/jpeg'
				&& uploadedFiles[0].type !== 'image/gif' && uploadedFiles[0].type !== 'image/jpg') {
					return res.badRequest('Error mimetype file');
				}
				if (uploadedFiles.length === 0) return res.badRequest('No file was uploaded');
				if (uploadedFiles[0].size === 0) return res.badRequest('No file was uploaded');
				mime(uploadedFiles[0].fd, function (err, type) {
					if (err) {
						// console.error(err.message);
						// ERROR: cannot open `/path/to/foo.pdf' (No such file or directory)
					} else {
						// console.log('Detected mime type: %s', type);
						// application/pdf
						if (type == 'image/png' || type == 'image/jpeg' || type == 'image/gif' || type == 'image/jpg') {
							const path = uploadedFiles[0].fd.split('assets/');
							if (path[1] === '') return res.badRequest('Path null');
							User.findOne({ id: req.session.passport.user }, (err, user) => {
								if (err) return done(err);
								if (!user) return res.redirect('/');
								if (user.image !== 'images/avatar/default.jpg'){
									// fs.unlink('./assets/' + user.image);
								}
								user.image = path[1];
								user.save((err) => {
									if (err) console.log('ERROR upload photo');
									return res.send({message: 'Image uploaded'});
								});
							});
						} else {
							return res.badRequest('No file was uploaded');
						}
					}
				});
			}
		);
	},

	myprofile: (req, res) => {
		User.findOne({ username: req.user.username }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.redirect('/');
			return res.view('myprofile', { user });
		});
	},

	profile: (req, res) => {
		User.findOne({ id: req.params.id }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.redirect('/');
			return res.view('profile', { user });
		});
	},

	// delete: (req, res) => {
	// 	User.findOne({ id: req.session.passport.user }, (err, user) => {
	// 		if (err) return done(err);
	// 		if (!user) return res.redirect('/');
	// 		if (user.image !== 'images/avatar/default.jpg') fs.unlink('./assets/' + user.image);
	// 		User.destroy({ id: req.session.passport.user }).exec((err) => {
	// 			if (err) return res.negotiate(err);
	// 			authController.logout(req, res);
	// 		});
	// 	});
	// },

	// SignIn with 42
	auth: (req, res) => {
		res.redirect (test.auth.code.getUri ())
	},

	'42/callback': (req, res) => {
		test.auth.code.getToken (req.originalUrl).then ((user) => {
			const request = require ('request');
			const accessToken = user.accessToken;
			request({
				url: 'https://api.intra.42.fr/v2/me',
				auth: {
					'bearer': accessToken,
				}
			}, (err, response) => {
				const body = JSON.parse (response.body)
				const random = randomstring.generate();
				User.findOne({ username: body.login + body.id}, (err, user) => {
					if (!err && user || user !== undefined ) {
						req.logIn(user, (error) => {
							if (error) return res.send(error);
							return res.redirect('/hypertube');
						});
					}
					else{
						User.findOne({ email: body.id + '@hypertube.fr' }, (err, user) => {
							// console.log(user)
							if (user) {
								req.logIn(user, (error) => {
									if (error) return res.send(error);
									return res.redirect('/hypertube');
								});
							} else {
								User.findOrCreate({
									Id42: body.id,
									username: body.login + body.id,
									lastName: body.last_name,
									firstName: body.first_name,
									email: body.id + '@hypertube.fr',
									image: body.image_url,
									langue: 'en',
									password: random,
									omniauth: true,
								}).exec((err, user) => {
									if (err || user === undefined) return res.redirect('/');
									req.logIn(user, (error) => {
										if (error) return res.send(error);
										return res.redirect('/hypertube');
									});
								})
							}
						});
					}
				});
			})
		})
	},

	reset: (req, res) => {
		User.findOne({ $or: [{ username: req.body.user }, { email: req.body.user }] }, (err, user) => {
			if (err) return done(err);
			if (!user) return res.send({ error: 'Username or email invalide' });
			if (user.omniauth === true)
			return res.send({ error: 'Can\'t reset password' });
			const token = mailer.sendMail(user.firstName, user.email);
			user.token = token
			user.save((err) => {
				if (err) console.error('ERROR!');
				return res.send({ success: 'Mail to reset your password sent' });
			});
		});
	},

	changePassword: (req, res) => {
		User.findOne({ token: req.params.token }, (err, user) => {
			if (user === undefined) return res.redirect('/');
			else {
				return res.view('newpassword', { tokens: req.params.token });
			}
		});
	},

	newPassword: (req, res) => {
		const regexPassword = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
		if (!req.body.password  || !regexPassword.test(req.body.password)) {
			return res.send({ error: 'Password too weak' });
		} else {
			User.findOne({ token: req.params.token }, (err, user) => {
				if (err || typeof user === 'undefined') return res.send ({ error: 'Error' });
				if (!req.body) return res.send ({ error: 'Error' });
				user.password = req.body.password;
				if (user.token === null) return res.send ({ error: 'Error' });
				bcrypt.genSalt(10, (err, salt) => {
					bcrypt.hash(user.password, salt, (error, hash) => {
						if (error) return res.send({ message: 'Error' })
						user.password = hash;
						user.token = null;
						user.save((err) => {
							if (err) return res.send({ message: 'Error' })
							return res.send ({ success: 'Password modified.' });
						});
					});
				});
			});
		}

	},

	passwordforgotten: (req, res) => {
		res.view('passwordforgotten');
	},
};
