/**
* APIController
*
* @description :: Server-side logic for managing APIS
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

import request	from 'request'

/*

request (`${req.protocol}://${req.get ('host')}/api/v1/getMovies?offset=${}&limit=${}&query=${}&genres=${}&actors=${}&directors=${}&parentalGuidances=${}&minRate=${}&maxRate=${}&minYear=${}&maxYear=${}&orderBy=${}&orderSens=${}&quality=${}`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains all movies found.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getGenres`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains all genres found.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getActors`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains all actors found.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getDirectors`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains all directors found.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getQualities`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains all qualities found.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getParentalGuidances`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains all parentalGuidances found.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getTrailers?movieId=${}`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains an array of trailers id of best seeded movies, randomized.
	// if a movieId is specified, the first index of the array contains the movieId's trailer id, if it exists.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/updateStats`, (err, response, body) => {
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getStats`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains stats:
	// parsedBody = {
	// 	rates: {
	// 		'1': x,
	// 		'2': x,
	// 		...
	// 		'10': x,
	// 	},
	// 	years: {
	// 		'1915': x,
	// 		'1916': x,
	// 		...
	// 		'2016': x,
	// 	},
	// }
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getHash?movieId=${}`, (err, response, body) => {
	// body contains the subtitles's hash.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getAvailableSubtitles?movieId=${}|hash=${}`, (err, response, body) => {
	const parsedBody = JSON.parse (body)

	// parsedBody contains an array with the subtitles's path.
})

request (`${req.protocol}://${req.get ('host')}/api/v1/getSubtitles?hash=${}&language=${}`, (err, response, body) => {
	// body contains the movie's subtitles.
})

request (`${req.protocol}://${req.get ('host')}/admin/updateDb`, () => {
	// Parses lasts pages of Yts, Yify and Tpb
})

request (`${req.protocol}://${req.get ('host')}/admin/updateDbAll`, () => {
	// Parses all pages of Yts, Yify and Tpb
})

*/

module.exports = {
	getMovies: (req, res) => {
		const options = {
			offset: req.query.offset !== 'undefined' && !isNaN (req.query.offset) && req.query.offset > 0 ? req.query.offset : 0,
			limit: req.query.limit !== 'undefined' && !isNaN (req.query.limit) && req.query.limit > 0 ? req.query.limit : 10,
			query: typeof req.query.query !== 'undefined' && req.query.query.length > 0 ? req.query.query : null,
			genres: req.query.genres !== 'null' && typeof req.query.genres !== 'undefined' && req.query.genres.length > 0 ? req.query.genres.split (',') : null,
			actors: typeof req.query.actors !== 'undefined' && req.query.actors.length > 0 ? req.query.actors.split (',') : null,
			directors: typeof req.query.directors !== 'undefined' && req.query.directors.length > 0 ? req.query.directors : null,
			parentalGuidances: typeof req.query.parentalGuidances !== 'undefined' && req.query.parentalGuidances.length > 0 ? req.query.parentalGuidances.split (',') : null,
			minRate: typeof req.query.minRate !== 'undefined' && !isNaN (req.query.minRate) ? req.query.minRate : 0,
			maxRate: typeof req.query.maxRate !== 'undefined' && !isNaN (req.query.maxRate) ? req.query.maxRate : 10,
			minYear: typeof req.query.minYear !== 'undefined' && !isNaN (req.query.minYear) ? req.query.minYear : 1900,
			maxYear: typeof req.query.maxYear !== 'undefined' && !isNaN (req.query.maxYear) ? req.query.maxYear : 2100,
			orderBy: typeof req.query.orderBy !== 'undefined' && req.query.orderBy.length > 0 && req.query.orderBy in Movie.attributes ? req.query.orderBy : 'title',
			orderSens: typeof req.query.orderSens !== 'undefined' && req.query.orderSens.length > 0 ? req.query.orderSens : 'asc',
			quality: typeof req.query.quality !== 'undefined' && req.query.quality.length > 0 ? req.query.quality : null,
		}

		console.time ('Duration')

		options.genres = options.genres ? options.genres.map ((genre) => {
			return genre.toLowerCase ().replace(/\b\w/g, l => l.toUpperCase())
		}) : null

		Movie.find ({
			where: {
				or: options.query ? [{
					title: {
						contains: options.query,
					},
				},
				{
					title: {
						like: options.query,
					},
				}] : [true],
				genres: options.genres ? options.genres : { like: '%' },
				actors: options.actors ? options.actors : { like: '%' },
				director: options.directors ? options.directors : { like: '%' },
				rate: {
					'>=': options.minRate,
					'<=': options.maxRate,
				},
				year: {
					'>=': options.minYear,
					'<=': options.maxYear,
				},
				'torrents.quality': options.quality ? options.quality : { like: '%' },
				parentalGuidance: options.parentalGuidances ? options.parentalGuidances : { like: '%' },
			},
			limit: options.limit,
			skip: options.offset,
			sort: `${options.orderBy} ${options.orderSens}`,
		}).exec ((err, movies) => {
			if (err) return console.error (err)

			console.log (`${movies.length} movies loaded.`)
			console.timeEnd ('Duration')

			return res.json (movies)
		})
	},

	getGenres: (req, res) => {
		Movie.native ((err, coll) => {
			if (err) return

			coll.distinct ('genres', (distinctErr, genres) => {
				if (distinctErr) res.end ()

				const index = genres.indexOf ('N/A')
				if (index >= 0) genres.splice (index, 1)
				res.json (genres)
			})
		})
	},

	getActors: (req, res) => {
		Movie.native ((err, coll) => {
			if (err) return

			coll.distinct ('actors', (distinctErr, actors) => {
				if (distinctErr) res.end ()

				res.json (actors)
			})
		})
	},

	getDirectors: (req, res) => {
		Movie.native ((err, coll) => {
			if (err) return

			coll.distinct ('director', (distinctErr, directors) => {
				if (distinctErr) res.end ()

				res.json (directors)
			})
		})
	},

	getQualities: (req, res) => {
		Movie.native ((err, coll) => {
			if (err) return

			coll.distinct ('torrents.quality', (distinctErr, qualities) => {
				if (distinctErr) res.end ()

				const properQualities = []

				qualities.forEach ((element) => {

					if (element && properQualities.indexOf (element) < 0) {
						properQualities.push (element)
					}
				})

				res.json (properQualities)
			})
		})
	},

	getParentalGuidances: (req, res) => {
		Movie.native ((err, coll) => {
			if (err) return

			coll.distinct ('parentalGuidance', (distinctErr, parentalGuidances) => {
				if (distinctErr) res.end ()

				res.json (parentalGuidances)
			})
		})
	},

	getTrailers: (req, res) => {
		request (`${req.protocol}://${req.get('host')}/api/v1/getMovies?orderBy=seed&orderSens=desc`, (requestReq, requestRes, requestBody) => {
			requestBody = JSON.parse (requestBody)
			const youtubeIds = []
			let getTrailers = (containsMovieTrailer) => {
				for (let movie in requestBody) {
					if (requestBody[movie].youtubeId && requestBody[movie].youtubeId.length === 11) {
						youtubeIds.push (requestBody[movie].youtubeId)
					}
				}
				for (let i = youtubeIds.length - 1 - (containsMovieTrailer ? 0 : 0); i >= (containsMovieTrailer ? 1 : 0); i--) {
					let randomIndex = Math.floor (Math.random () * (i + 1) + 1)
					let itemAtIndex = youtubeIds[randomIndex]

					youtubeIds[randomIndex] = youtubeIds[i]
					youtubeIds[i] = itemAtIndex
				}
			}

			if (req.query.movieId && typeof req.query.movieId !== 'undefined') {
				Movie.findOne ({ id: req.query.movieId }, (err, movie) => {
					if (movie && movie.youtubeId && movie.youtubeId.length === 11) {
						youtubeIds.push (movie.youtubeId)
						getTrailers (true)
					} else {
						getTrailers ()
					}
					return res.json (youtubeIds)
				})
			} else {
				getTrailers ()
				return res.json (youtubeIds)
			}
		})
	},

	updateStats: (req, res) => {
		const stats = {}
		stats['rates'] = {}

		for (let rate = 0; rate <= 10; rate++) {
			Movie.count ({rate: {$gte: rate - 0.5, $lt: rate + 0.5}}, (err, count) => {
				stats.rates[rate.toString ()] = count
				if (rate == 10) {
					const currentYear = new Date ().getFullYear ()
					stats['years'] = {}
					let lastYear = 0
					for (let year = 1915; year <= currentYear; year++) {
						Movie.count ({year: year}, (err, count) => {
							if (year % 2 == 0) {
								stats.years[year.toString ()] = count + lastYear
							} else {
								lastYear = count
							}
							if (year == currentYear) {
								Stat.actualize (stats)
							}
						})
					}
				}
			})
		}
		return res.end ('Updating stats.')
	},

	getStats: (req, res) => {
		const stats = {}

		Stat.find ({title: 'rates'}, (err, rates) => {
			if (err) { return res.end ('{}') }

			stats['rates'] = rates
			Stat.find ({title: 'years'}, (err, years) => {
				if (err) { return res.end ('{}') }

				stats['years'] = years
				return res.json (stats)
			})
		})
	},

	getHash: (req, res) => {
		Movie.findOne ({ _id: req.query.movieId && typeof req.query.movieId !== 'undefined' && req.query.movieId.length === 24 ? req.query.movieId : -1 }, (err, movie) => {
			if (err || !movie || movie.torrents.length <= 0 || !movie.torrents[0].magnet) { return res.end ('') }

			if (movie.subtitlesHash) { return res.end (movie.subtitlesHash) }

			getSubtitles.getHash (movie.torrents[0].magnet, (err, hash) => {
				if (err) { return res.end ('') }

				movie.subtitlesHash = hash
				movie.save ()
				return res.end (hash)
			})
		})
	},

	getAvailableSubtitles: (req, res) => {
		let movieId = req.query.movieId && typeof req.query.movieId !== 'undefined' && req.query.movieId.length === 24 ? req.query.movieId : null
		let hash = req.query.hash && typeof req.query.hash !== 'undefined' && req.query.hash.length === 32 ? req.query.hash : null

		if (!hash && !movieId) return res.end ('You need to specify a hash or a movieId.')

		const getSubtitlesFunction = (hash) => {
			getSubtitles.sendRequest ({
				action: 'search',
				hash: hash,
			}, (err, body) => {
				if (err) console.error (err)

				res.json (err || !body ? [] : body.split (',').map ((language) => {
					return `/api/v1/getSubtitles?hash=${hash}&language=${language}`
				}))
			})
		}

		if (!hash) {
			Movie.findOne ({ id: movieId }, (err, movie) => {
				if (err || !movie || movie.torrents.length <= 0 || !movie.torrents[0].magnet) { return res.end ('') }

				if (movie.subtitlesHash) { return getSubtitlesFunction (movie.subtitlesHash) }

				getSubtitles.getHash (movie.torrents[0].magnet, (err, hash) => {
					if (err) { return res.end ('') }

					movie.subtitlesHash = hash
					movie.save ()
					return getSubtitlesFunction (hash)
				})
			})
		} else {
			return getSubtitlesFunction (hash)
		}
	},

	getSubtitles: (req, res) => {
		const hash = req.query.hash && typeof req.query.hash !== 'undefined' && req.query.hash.length === 32 ? req.query.hash : null
		const language = req.query.language && typeof req.query.language !== 'undefined' && req.query.language.length === 2 ? req.query.language : null

		if (!hash || !language) return res.end ('You need to specify a hash.')

		getSubtitles.sendRequest ({
			action: 'download',
			hash: hash,
			language: language || 'en',
		}, (err, body) => {
			if (err || !body) return console.error (err || 'not found.')

			body = body.replace (/([0-9]{2}:){2}[0-9]{2},[0-9]{3}/g, (match) => { return match.replace (',', '.') })
			if (body[0] !== '1')
			body = body.substr (1)
			body = `WEBVTT\n\n` + body

			res.setHeader('Content-Type', 'application/octet-stream');
			res.end (body)
		})
	},

	updateDb: (req, res) => {
		console.time ('Tpb parsed')
		getMovies.fromTpb ({
			firstPage: 0,
			nbOfPages: 20,
			query: '',
			timeout: 60 * 1000,
		}, (movies) => {
			console.timeEnd ('Tpb parsed')
			console.time ('Yts parsed')
			getMovies.fromYts ({
				firstPage: 0,
				nbOfPages: 20,
				query: '',
				timeout: 60 * 1000,
			}, (movies) => {
				console.timeEnd ('Yts parsed')
				console.time ('Yify parsed')
				getMovies.fromYify ({
					firstPage: 0,
					nbOfPages: 10,
					query: '',
					timeout: 60 * 1000,
				}, (movies) => {
					console.timeEnd ('Yify parsed')
				})
			})
		})
		res.end ('Parsing few pages of Tpb, Yifi and Yts.')
	},

	updateDbAll: (req, res) => {
		// Check if user is admin
		console.time ('Tpb parsed')
		getMovies.fromTpb ({
			firstPage: 0,
			nbOfPages: 200,
			query: '',
			timeout: 60 * 1000,
		}, (movies) => {
			console.timeEnd ('Tpb parsed')
			console.time ('Yts parsed')
			getMovies.fromYts ({
				firstPage: 0,
				nbOfPages: 500,
				query: '',
				timeout: 120 * 1000,
			}, (movies) => {
				console.timeEnd ('Yts parsed')
				console.time ('Yify parsed')
				getMovies.fromYify ({
					firstPage: 0,
					nbOfPages: 250,
					query: '',
					timeout: 120 * 1000,
				}, (movies) => {
					console.timeEnd ('Yify parsed')
				})
			})
		})
		res.end ('Updating database. May take a while.')
	},

	clearMovies: (req, res) => {
		Movie.destroy ().exec ((err) => {
			if (err) { return res.end ('Failed to clear Movie document.') }

			return res.end ('Movie document cleared.')
		})
	},
}
