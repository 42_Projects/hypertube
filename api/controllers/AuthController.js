import passport from 'passport';
/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	_config: {
		actions: false,
		shortcuts: false,
		rest: false,
	},

	// SignIn
	login: (req, res) => {
		passport.authenticate('local', (err, user, info) => {
			if ((err) || (!user)) {
				return res.send({
					message: info.message,
				});
			}
			req.session.languagePreference = user.langue;
			req.logIn(user, (error) => {
				if (error) res.send(error);
				res.redirect('/upload');
			});
		})(req, res);
	},

	// SignIn with Facebook
	facebook: (req, res, next) => {
		passport.authenticate('facebook', { scope: ['public_profile', 'user_events', 'email'] })(req, res, next);
	},
	'facebook/callback': (req, res, next) => {
		passport.authenticate('facebook',
		(err, user) => {
			req.logIn(user, (error) => {
				if (error) {
					res.redirect('/');
				} else {
					res.redirect('/hypertube');
				}
			});
		})(req, res, next);
	},

	// SignIn with Github
	github: (req, res, next) => {
		passport.authenticate('github', { scope: ['user:email'] })(req, res, next);
	},
	'github/callback': (req, res, next) => {
		passport.authenticate('github', { scope: ['user:email'] },
		(err, user) => {
			req.logIn(user, (erro) => {
				if (erro) {
					res.redirect('/');
				} else {
					res.redirect('/hypertube');
				}
			});
		})(req, res, next);
	},

	// SignIn with Twitter
	twitter: (req, res, next) => {
		passport.authenticate('twitter')(req, res, next);
	},
	'twitter/callback': (req, res, next) => {
		passport.authenticate('twitter',
		(err, user) => {
			req.logIn(user, (erro) => {
				if (erro) {
					res.redirect('/');
				} else {
					res.redirect('/hypertube');
				}
			});
		})(req, res, next);
	},

	// SignIn with Google
	google: (req, res, next) => {
		passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read'] })(req, res, next);
	},
	'google/callback': (req, res, next) => {
		passport.authenticate('google',
		(err, user) => {
			req.logIn(user, (erro) => {
				if (erro) {
					res.redirect('/');
				} else {
					res.redirect('/hypertube');
				}
			});
		})(req, res, next);
	},

	// Logout
	logout: (req, res) => {
		req.session.languagePreference = 'en';
		req.logout();
		res.redirect('/');
	},
 };
