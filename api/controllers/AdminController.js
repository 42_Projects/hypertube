import fs from 'fs';

/**
* AdminController
*
* @description :: Server-side logic for managing admins
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

module.exports = {
	admin: (req, res) => {
		Movie.find((err, movies) => {
			if (err) return res.view('admin', {message: 'Error with Movie'});
			if (!movies) return res.view('admin', {message: 'Error with Movie'});
			const nbMovies = movies.length;
			fs.readdir('./movie/', (err, files) => {
				let nbDownload = 0;
				if (files !== undefined) nbDownload = files.length;
				User.find((err, users) => {
					if (err) return res.view('admin', {message: 'Error with Users'});
					if (!users) return res.view('admin', {message: 'Error with Users'});
					let tabUsers = [];
					tabUsers[0] = users.length;
					tabUsers[1] = 0;
					tabUsers[2] = 0;
					tabUsers[3] = 0;
					tabUsers[4] = 0;
					tabUsers[5] = 0;
					tabUsers[6] = 0;
					tabUsers[7] = 0;
					// let nbConnecter = [0];
					for (var prop in req.sessionStore.sessions){
						var tabs = JSON.parse(req.sessionStore.sessions[prop]);
						// console.log(tabs);
						if (tabs.passport) tabUsers[1]++;
					}
					for(var prop in users){
						// console.log(prop);
						if (users[prop].langue === 'en') tabUsers[2]++;
						if (users[prop].langue === 'es') tabUsers[3]++;
						if (users[prop].langue === 'fr') tabUsers[4]++;
						if (users[prop].langue === 'de') tabUsers[5]++;
						if (users[prop].langue === 'it') tabUsers[6]++;
						if (users[prop].omniauth === true) tabUsers[7]++;
					}

					Stat.find((err, stats) => {
						if (err) return res.view('admin', {message: 'Error with Stats'});
						if (!stats) return res.view('admin', {message: 'Error with Stats'});
						let rates
						let years
						for (let stat in stats) {
							if (stats[stat].title === 'years') {
								years = stats[stat]
							}
							if (stats[stat].title === 'rates') {
								rates = stats[stat]
							}
						}
						const ratesData = rates ? rates.data : [];
						const ratesLabels = rates ? rates.labels : [];
						const yearsLabels = years ? years.labels : [];
						const yearsData = years ? years.data : [];

						return res.view('admin', {
							nbMovies,
							nbDownload,

							tabUsers,

							ratesData,
							ratesLabels,
							yearsLabels,
							yearsData,
						});
					});
				});
			});
		});
	},
};
