import bcrypt from 'bcrypt';

/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
*/

module.exports = {
	attributes: {
		Id42: {
			type: 'string',
		},

		twitterId: {
			type: 'string',
		},

		googleId: {
			type: 'string',
		},

		githubId: {
			type: 'string',
		},

		facebookId: {
			type: 'string',
		},

		username: {
			type: 'string',
			minLength: 1,
			unique: true,
		},

		lastName: {
			type: 'string',
			minLength: 2,
		},

		firstName: {
			type: 'string',
			minLength: 2,
		},

		email: {
			type: 'email',
			unique: true,
		},

		image: {
			type: 'string',
		},

		password: {
			type: 'string',
			minLength: 6,
		},

		token: {
			type: 'string',
		},

		langue: {
			type: 'string',
		},

		movieView: {
			type: 'array',
		},

		omniauth: {
			type: 'boolean',
		},
	},

	beforeCreate: (user, cb) => {
		bcrypt.genSalt(10, (err, salt) => {
			bcrypt.hash(user.password, salt, (error, hash) => {
				if (error) {
					cb(error);
				} else {
					user.password = hash;
					cb();
				}
			});
		});
	},
};
