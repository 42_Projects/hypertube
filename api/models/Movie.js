/**
* Movie.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
*/

module.exports = {

	attributes: {

		title: {
			type: 'string',
		},

		year: {
			type: 'integer',
		},

		quality: {
			type: 'string',
		},

		runtime: {
			type: 'string',
		},

		genres: {
			type: 'array',
		},

		director: {
			type: 'string',
		},

		actors: {
			type: 'array',
		},

		plot: {
			type: 'string',
		},

		awards: {
			type: 'string',
		},

		poster: {
			type: 'string',
		},

		imdbId: {
			type: 'string',
		},

		youtubeId: {
			type: 'string',
		},

		subtitles: {
			type: 'array',
		},

		rate: {
			type: 'float',
		},

		parentalGuidance: {
			type: 'string',
		},

		subtitlesHash: {
			type: 'string',
		},

		seed: {
			type: 'integer',
		},

		torrents: {
			type: 'array',
		},

		path: {
			type: 'string',
		},

		lastView: {
			type: 'date',
		},

		commentaires: {
			type: 'array',
		},
	},

	findOrCreateUser: (options, callback) => {
		Movie.findOne ({
			title: options.title,
			year: parseInt (options.year),
		}, (err, movie) => {
			if (err) return callback ? callback (null) : true
			if (movie) {
				let qualityExists = false
				let needToBeSaved = false
				for (let i = 0; i < movie.torrents.length; i += 1) {
					for (let k = 0; k < options.torrents.length; k += 1) {
						if (movie.torrents[i].quality === options.torrents[k].quality) {
							qualityExists = true
							if (movie.seed < options.torrents[k].seed) {
								movie.seed = options.torrents[k].seed
								needToBeSaved = true
							}
							if (movie.torrents[i].seed < options.torrents[k].seed &&
								movie.torrents[i].magnet !== options.torrents[k].magnet) {
								movie.torrents[i] = options.torrents[k]
								needToBeSaved = true
							}
						}
					}
				}
				if (needToBeSaved) {
					movie.seed = Math.max.apply (null, movie.torrents.map(torrent => torrent.seed))
					movie.save ()
				}
				return callback ? callback (movie, true) : true
			} else {
				return Movie.createMovie (options, callback)
			}

		})
	},

	createMovie: (options, callback) => {
		const rate = parseFloat (options.rate)
		Movie.create ({
			title: options.title,
			year: parseInt (options.year),
			runtime: options.runtime,
			genres: options.genres,
			director: options.director,
			actors: options.actors,
			plot: options.plot,
			awards: options.awards,
			poster: options.poster,
			youtubeId: options.youtubeId,
			imdbId: options.imdbId,
			subtitles: options.subtitles,
			rate: !isNaN (rate) ? rate : -1,
			parentalGuidance: options.parentalGuidance,
			torrents: options.torrents,
			seed: Math.max.apply (null, options.torrents.map(torrent => torrent.seed))
		}, (err, created) => {
			if (err) return console.error (err)
			if (callback) { callback (created, false) }

			return true
		})
		return true
	},
}
