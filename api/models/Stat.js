/**
* Stat.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
*/

module.exports = {

	attributes: {
		title: {
			type: 'string'
		},

		labels: {
			type: 'array',
		},

		data: {
			type: 'array',
		},

		// moviesByYear: {
		// 	years: {
		// 	},
		//
		// 	movies: {
		// 		type: 'array',
		// 	},
		// },
		//
		// moviesByRate: {
		// 	rates: {
		// 		type: 'array',
		// 	},
		//
		// 	movies: {
		// 		type: 'array',
		// 	},
		// },
	},

	actualize: (data) => {
		if (!data) { return }

		const rates = {rates: [], movies: []}
		const years = {years: [], movies: []}

		for (let rate in data.rates) {
			rates.rates.push (parseInt (rate))
			rates.movies.push (data.rates[rate])
		}

		for (let year in data.years) {
			years.years.push (parseInt (year))
			years.movies.push (data.years[year])
		}

		Stat.findOrCreate ({title: 'rates'}, {title: 'rates', labels: rates.rates, data: rates.movies}).exec (() => {
		})
		Stat.findOrCreate ({title: 'years'}, {title: 'years', labels: years.years, data: years.movies}).exec (() => {
		})
	}
}
