module.exports = (req, res, next) => {
	if (req.session.languagePreference === undefined)
		req.session.languagePreference = 'en';
	req.setLocale(req.session.languagePreference);
	next();
};
