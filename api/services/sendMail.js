import nodemailer from 'nodemailer';

const randomstring = require('randomstring');

module.exports = {
	sendMail: (prenom, email) => {
		const token = randomstring.generate();
		const transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: 'apimatcha@gmail.com', // Your email id
				pass: 'apiMatcha1212' // Your password
			}
		});
		const text = 'Bonjour' + prenom + '\n\n Vous pouvez reinitialiser votre email en cliquant sur le lien\n\n' +
		'http://localhost:1337/change-password/' + token;
		const mailOptions = {
			from: 'apimatcha@gmail.com', // sender address
			to: email, // list of receivers
			subject: 'Forgot password ?', // Subject line
			text
		};
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) console.log(error);
			else console.log('Message sent: ' + info.response);
		});
		return (token);
	},
}
