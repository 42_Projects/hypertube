import fs		from 'fs'
import crypto	from 'crypto'
import request	from 'request'
import torrent	from 'torrent-stream'

module.exports = {
	getHash: (magnet, callback) => {
		const buffSize = 64 * 1024
		const buff = new Buffer (buffSize * 2)
		let timeouted = false

		const engine = torrent (magnet);

		let timeout = setTimeout (() => {
			timeouted = true
			callback (true)
		}, 60 * 1000)

		if (!magnet) { return callback (true) }

		engine.on ('ready', () => {
			engine.files.forEach ((file) => {
				if (file.length < 100000000) return

				let streamStart = file.createReadStream({
					start: 0,
					end: buffSize - 1,
				})
				let streamEnd;

				streamStart.on ('data', (dataStart) => {
					if (dataStart.length >= buffSize) {
						streamEnd = file.createReadStream({
							start: file.length - buffSize,
							end: file.length - 1,
						});

						streamEnd.on ('data', (dataEnd) => {
							if (!timeouted) {
								if (dataStart.length !== buffSize || dataEnd.length !== buffSize) { return callback (true) }

								const hash = crypto.createHash ('md5').update (Buffer.concat ([dataStart, dataEnd], buffSize * 2)).digest ('hex')
								return callback (false, hash)
							}
						})
					}
				});
			})
		})
	},

	sendRequest: (options, callback) => {
		const opts = {
			action: options.action && ['search', 'download'].indexOf (options.action) >= 0 ? options.action : 'download',
			hash: options.hash ? options.hash : null,
			language: options.language && /^[a-z]{2}$/.test (options.language) ? options.language : 'en',
		}
		if (!opts.hash) {
			if (callback) return callback ('You need to specify a hash.')
			return
		}

		const requestOptions = {
			url: `http://api.thesubdb.com/?action=${opts.action}&hash=${opts.hash}&language=${opts.language}`,
			headers: {
				'User-Agent': 'SubDB/1.0 (Hypertube/1.0; https://gitlab.com/hypertube42/42)',
				'Content-Type': 'application/octet-stream'
			},
		}

		request (requestOptions, (err, res, body) => {
			if (err || res.statusCode !== 200) return callback (err)
			return callback (null, body)
		})
	},
}
