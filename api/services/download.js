import torrentStream from 'torrent-stream';
import valideExtension from './extension';
import moment from 'moment'

const torrent = (res, req, id, quality, movie, stream, path) => {
	return new Promise((fulfill, reject) => {
		if (movie === null) return reject({error: 'No valid id'});
		let isDownloading = false
		let currentTorrent

		movie.torrents.forEach((torrent) => {
			if (torrent.quality === quality) {
				currentTorrent = torrent
				if (torrent.isDownloading) {
					console.log (`[Already downloading: ${movie.title}]`.orange)
					isDownloading = true
				} else {
					console.log (`[Will be downloaded: ${movie.title}]`.green)
					torrent.isDownloading = true
					movie.save().then(
						(ret_movies) => {
							movie.torrents.forEach((torrent) => {
								if (torrent.quality === quality) {
								}
							})
						}, (err) => {
						}
					)
				}
				if (isDownloading) {
					return fulfill (torrent && torrent.tmpPath && /mp4$/.test (torrent.tmpPath) ? {
						path: torrent.tmpPath
					} : false)
				}
			}
		})

		let start = new Date ()
		let lastSize = 0

		console.log (`[Starting download]`.blue)
		const engine = torrentStream(stream.magnet, { path });
		engine.on('ready', () => {
			let movieFile;
			let lastIndex = 0
			engine.files.forEach((file) => {
				if (valideExtension(file.name) && (!movieFile || file.length > movieFile.length)) {
					movieFile = file;
					if (movieFile) {
						movie.torrents.forEach((torrent) => {
							if (torrent.quality === quality) {
								torrent.tmpPath = file.path; // ❗️
							}
						})
					}
					// console.log(`[${file.name} will be downloaded]`.green);
				} else {
					// console.log(`[${file.name} will be ignored]`.red);
				}
			});
			if (movieFile) {
				movieFile.select();
				currentTorrent.path = movieFile.path
				movie.save ()
				const movieData = {
					name: movieFile.name,
					length: movieFile.length,
					date: movieFile.released,
					path: movieFile.path
				};
				console.time (`[Time to download ${movieData.name}]`.green)

				fulfill(movieData);
				engine.on('download', (index) => {
					if (index % 10 === 0) {
						var dataToFront = {};
						const percent = parseInt (engine.swarm.downloaded / movieFile.length * 10000) / 100
						const eta = moment ().to (moment (moment () + parseInt (((new Date () - start)) / ((engine.swarm.downloaded - lastSize)) * (movieFile.length - engine.swarm.downloaded))))
						const speed = parseInt ((engine.swarm.downloaded - lastSize) / (new Date () - start)) / 1000
						console.log(`${movie.title} [ ${quality} ]: ${percent} % | ETA: ${eta} | ${speed} Mo/s`)
						start = new Date ()
						lastSize = engine.swarm.downloaded
						dataToFront.percent = percent
						dataToFront.eta = eta
						dataToFront.speed = speed
						sails.sockets.blast(id, dataToFront, req)
						movie.torrents.forEach((torrent) => {
							if (torrent.quality === quality) {
								torrent.percent = percent; // ❗️
							}
						})
						movie.save ()
					}
				});
				engine.on('idle', () => {
					console.timeEnd (`[Time to download ${movieData.name}]`.green)
					engine.removeAllListeners();
					engine.destroy();

					sails.sockets.blast(id, { percent: 100, eta: '', speed: '0' }, req);
					movie.torrents.forEach((resolution) => {
						if (resolution.quality === quality) {
							resolution.path = movieFile.path;
							resolution.percent = 100
							resolution.isDownloading = false
							currentTorrent = resolution
						}
					})
					movie.save().then(
						(ret_movies) => {
							// console.log('Path saved: ' + movieFile.path);
							return fulfill(true);
						},
						(err) => {
							return fulfill(false);
						}
					);
				});

			} else {
				// console.log('Le film est introuvable'.bgRed);
				engine.removeAllListeners();
				engine.destroy();
				return reject({
					message: 'No valid movie file was found.'
				});
			}
		});
	});
};

module.exports = torrent;
