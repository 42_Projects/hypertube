const oauth = require ('client-oauth2');

const uid = '61bb67a85c028ee0d42655c06e95339f6502c5e7d7c64d1b85b5e85a41f78168';
const secret = '6f8ee1bb15750caaf4d3d0666c769cdd9d70a30c8406d04f181c7dabf79b1a79';
const auth = new oauth ({
	clientId: uid,
	clientSecret: secret,
	accessTokenUri: 'https://api.intra.42.fr/oauth/token',
	authorizationUri: 'https://api.intra.42.fr/oauth/authorize',
	redirectUri: 'http://localhost:1337/auth/42/callback',
});

export { oauth, uid, secret, auth };
