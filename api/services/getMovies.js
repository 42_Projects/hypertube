import request						from 'request'
import cheerio						from 'cheerio'
import omdb							from 'omdb-client'
import {XmlEntities as entities}	from 'html-entities'

/*

options = {
	query: opts.query || null,
	nbOfPages: opts.nbOfPages || 1,
	firstPage: opts.firstPage || 0,
	timeout: opts.timeout || 60 * 1000,
}

*/

module.exports = {
	fromTpb: (options, callback) => {
		let cb				= callback
		let opts			= options
		let path			= ''
		let elementsByPage	= null
		let timeouted		= false
		const movies		= []
		const counter		= {
			i: -1,
			j: -1,
			k: -1,
		}

		if (!cb) {
			if (!options) {
				cb = () => {}
				opts = {
					query: null,
					page: 0,
				}
			} else if (typeof options === 'function') {
				cb = options
				opts = {
					query: null,
					page: 0,
				}
			} else {
				cb = () => {}
			}
		}

		opts = {
			query: opts.query || null,
			nbOfPages: opts.nbOfPages || 1,
			firstPage: opts.firstPage || 0,
			timeout: opts.timeout || 60 * 1000,
		}

		const timeOut = setTimeout (() => {
			console.log (`Tpb get stopped after ${opts.timeout / 1000}s.`)
			timeouted = true
			return cb (movies)
		}, opts.timeout)

		for (let currentPage = opts.firstPage; currentPage <= (opts.firstPage + opts.nbOfPages) - 1 && currentPage <= 20; currentPage += 1) {
			if (opts.query && opts.query.length > 0) {
				path = `https://thepiratebay.org/search/${opts.query}/${currentPage}/0/200`
			} else {
				path = `https://thepiratebay.org/browse/201/${currentPage}/0`
			}

			request (path, (requestErr, res, body) => {
				if (requestErr || res.statusCode !== 200) { return console.error (`Tpb: ${requestErr || res.statusCode}`) }

				const $ = cheerio.load (body)

				if (!elementsByPage) {
					elementsByPage = $ ('.detName ~ a:first-of-type') ? $ ('.detName ~ a:first-of-type').length : 0
				}

				$ ('.detName ~ a:first-of-type').each ((index, element) => {
					if (!element) { return }
					const magnet = $ (element).attr ('href')
					const seed = $ (element).parent () ? $ (element).parent ().parent () ? $ (element).parent ().parent ().find ('td:nth-child(3)') ? $ (element).parent ().parent ().find ('td:nth-child(3)').text () || -1 : -1 : -1 : -1
					const leech = $ (element).parent () ? $ (element).parent ().parent () ? $ (element).parent ().parent ().find ('td:nth-child(4)') ? $ (element).parent ().parent ().find ('td:nth-child(4)').text () || -1 : -1 : -1 : -1
					let name = unescape (magnet).match (/dn=[a-zA-Z0-9-\[\]\(\):'.\+]+/)
					let quality

					if (!(/dn=[a-zA-Z0-9-\[\]\(\):'.\+]+/).test (unescape (name))) return

					name = unescape (name).match (/dn=[a-zA-Z0-9-\[\]\(\):'.\+]+/)[0].replace (/[-.\+]+/g, ' ').replace (/^dn= ?/, '')

					quality = name.match (/(720p?)|(1080p?)|(hd\w+)|(dvd\w+)|(webrip)|(camts)/i)
					if (quality) {
						quality = quality[0]
					}
					if (/(19|20)[0-9]{2}/.test (name)) {
						name = name.split (/\(?\[?(19|20)[0-9]{2}\)?\]?/)[0]
					}
					if (/(720p?)|(1080p?)|(hd\w+)|(dvd\w+)|(webrip)|(camts)/i.test (name)) {
						name = name.split (/(720p?)|(1080p?)|(hd\w+)|(dvd\w+)|(webrip)|(camts)/i)[0]
					}

					counter.j += 1;

					((query, movieInfos) => {
						omdb.get ({
							title: query,
						}, (omdbErr, movie) => {
							counter.k += 1

							if (!omdbErr && movie) {
								let alreadyExists = false
								movieInfos.quality = movieInfos.quality ? movieInfos.quality.replace (/^[a-zA-Z]+/, (x) => {
									return x.toUpperCase ()
								}).replace (/^[0-9]+\w+/, (x) => {
									return x.toLowerCase ()
								}) : 'HD'
								for (let id = 0; id < movies.length; id += 1) {
									if (movies[id].title === movie.Title && movies[id].year === movie.Year) {
										alreadyExists = true
										let qualityExists = false
										for (let torrentsId = 0; torrentsId < movies[id].torrents.length; torrentsId += 1) {
											if (movies[id].torrents[torrentsId].quality === movieInfos.quality || 'HD') {
												qualityExists = true
											}
										}
										if (!qualityExists) {
											movies[id].torrents.push ({
												quality: movieInfos.quality || 'HD',
												seed: movieInfos.seed,
												leech: movieInfos.leech,
												magnet: movieInfos.magnet,
												isDownloading: false,
												percent: 0,
											})
										}
									}
								}
								if (!alreadyExists) {
									const newMovie = {
										title: movie.Title,
										year: movie.Year,
										runtime: movie.Runtime,
										genres: movie.Genre.split (/ ?, ?/),
										director: movie.Director.replace ('N/A', ''),
										actors: movie.Actors.split (/ ?, ?/),
										plot: movie.Plot,
										awards: movie.Awards,
										poster: movie.Poster,
										imdbId: movie.imdbID,
										rate: movie.imdbRating,
										parentalGuidance: movie.Rated.replace ('N/A', ''),

										torrents: [{
											quality: movieInfos.quality || 'HD',
											seed: movieInfos.seed,
											leech: movieInfos.leech,
											magnet: movieInfos.magnet,
											isDownloading: false,
											percent: 0,
										}],
									}
									movies.push (newMovie)
									Movie.findOrCreateUser (newMovie, () => {
									})
								}
							}
							if (!timeouted && (counter.j === counter.k && counter.k >= elementsByPage * (opts.nbOfPages - 1))) {
								timeouted = true
								clearTimeout (timeOut)
								return cb (movies)
							}
							return true
						})
					}) (name, {
						magnet,
						quality,
						seed,
						leech,
					})
				})
			})
		}
	},

/*

options = {
	query: opts.query || 0,
	nbOfPages: opts.nbOfPages || 1,
	firstPage: opts.firstPage || 1,
	elementsByPage: opts.elementsByPage || elementsByPage,
	timeout: opts.timeout || 60 * 1000,
}

*/

	fromYts: (options, callback) => {
		let cb					= callback
		let opts				= options
		let path				= ''
		let timeouted			= false
		const elementsByPage	= 30
		const movies			= []
		const counter			= {
			i: 0,
			j: 0,
			k: 0,
		}

		if (!cb) {
			if (!options) {
				cb = () => {}
				opts = {
					query: null,
					page: 0,
				}
			} else if (typeof options === 'function') {
				cb = options
				opts = {
					query: null,
					page: 0,
				}
			} else {
				cb = () => {}
			}
		}

		opts = {
			query: opts.query || 0,
			nbOfPages: opts.nbOfPages || 1,
			firstPage: opts.firstPage || 1,
			elementsByPage: opts.elementsByPage || elementsByPage,
			timeout: opts.timeout || 60 * 1000,
		}

		const timeOut = setTimeout (() => {
			console.log (`Yts get stopped after ${opts.timeout / 1000}s.`)
			timeouted = true
			return cb (movies)
		}, opts.timeout)

		for (let currentPage = opts.firstPage; currentPage <= (opts.firstPage + opts.nbOfPages) - 1; currentPage += 1) {
			path = `https://yts.ag/api/v2/list_movies.json?query_term=${opts.query}&limit=${opts.elementsByPage}&page=${currentPage}`

			request (path, (requestErr, requestRes, body) => {
				if (requestErr || requestRes.statusCode != 200 || body[0] !== '{') { return console.error (`Yts: ${requestErr || requestRes.statusCode}`) }

				const parsedBody = JSON.parse (body)

				if (parsedBody.status !== 'ok' || !parsedBody.data || !parsedBody.data.movies) { return }

				for (let i = 0; i < parsedBody.data.movies.length; i += 1) {

					if (!parsedBody.data.movies[i] || !parsedBody.data.movies[i].torrents) { continue }

					const torrents = []

					for (let k = 0; k < parsedBody.data.movies[i].torrents.length; k += 1) {
						const magnet = `magnet:?xt=urn:btih:${parsedBody.data.movies[i].torrents[k].hash}&dn=${parsedBody.data.movies[i].url.split ('').reverse ().join ('').split ('/')[0].split ('').reverse ().join ('')}&tr=udp://tracker.openbittorrent.com:80&tr=udp://tracker.opentrackr.org:1337/announce`
						torrents.push ({
							quality: parsedBody.data.movies[i].torrents[k].quality ? parsedBody.data.movies[i].torrents[k].quality.replace (/^[a-zA-Z]+/, (x) => {
								return (x.toUpperCase ())
							}) : 'HD',
							seed: parsedBody.data.movies[i].torrents[k].seeds,
							leech: parsedBody.data.movies[i].torrents[k].peers,
							magnet,
							isDownloading: false,
							percent: 0,
						})
					}

					const imdbPath = `http://www.omdbapi.com/?i=${parsedBody.data.movies[i].imdb_code}&plot=short&r=json`

					request (imdbPath, (imdbErr, imdbResponse, imdbBody) => {
						counter.i += 1

						if (imdbErr || imdbBody[0] !== '{') { return }

						const imdbParsedBody = JSON.parse (imdbBody)
						if (!imdbParsedBody) { return }

						const newMovie = {
							title: parsedBody.data.movies[i].title,
							year: parsedBody.data.movies[i].year,
							runtime: parsedBody.data.movies[i].runtime,
							genres: parsedBody.data.movies[i].genres,
							director: imdbParsedBody.Director.replace ('N/A', ''),
							actors: imdbParsedBody.Actors.split (/ ?, ?/),
							plot: parsedBody.data.movies[i].description_full,
							awards: imdbParsedBody.Awards,
							poster: parsedBody.data.movies[i].large_cover_image,
							youtubeId: parsedBody.data.movies[i].yt_trailer_code,
							imdbId: parsedBody.data.movies[i].imdbID,
							rate: parsedBody.data.movies[i].rating,
							parentalGuidance: imdbParsedBody.Rated.replace ('N/A', ''),
							torrents,
						}
						movies.push (newMovie)

						Movie.findOrCreateUser (newMovie, (movie) => {
							counter.k += 1
							if (!timeouted && (counter.k >= parsedBody.data.movie_count || counter.k >= opts.elementsByPage * (opts.nbOfPages))) {
								timeouted = true
								clearTimeout (timeOut)
								return callback (movies)
							}
						})
					})
				}
			})
		}
	},

	fromYify: (options, callback) => {
		let cb				= callback
		let opts			= options
		let path			= ''
		let elementsByPage	= null
		let timeouted		= false
		const url			= 'https://www.yify-torrent.org'
		const movies		= []
		const counter		= {
			j: -1,
			k: -1,
		}

		if (!cb) {
			if (!options) {
				cb = () => {}
				opts = {
					query: null,
					page: 0,
				}
			} else if (typeof options === 'function') {
				cb = options
				opts = {
					query: null,
					page: 0,
				}
			} else {
				cb = () => {}
			}
		}

		opts = {
			query: opts.query || null,
			nbOfPages: opts.nbOfPages || 1,
			firstPage: opts.firstPage || 0,
			timeout: opts.timeout || 60 * 1000,
		}

		const timeOut = setTimeout (() => {
			console.log (`Yify get stopped after ${opts.timeout / 1000}s.`)
			timeouted = true
			return cb (movies)
		}, opts.timeout)


		for (let currentPage = opts.firstPage; currentPage <= (opts.firstPage + opts.nbOfPages) - 1; currentPage += 1) {
			if (opts.query && opts.query.length > 0) {
				path = `${url}/search/${opts.query}/${currentPage > 0 ? `t-${currentPage + 1}` : ''}`
			} else {
				path = `${url}/search/mp4/${currentPage > 0 ? `t-${currentPage + 1}/` : ''}`
			}


			request (path, (requestErr, requestRes, requestBody) => {
				if (requestErr || requestRes.statusCode !== 200) { return console.error (`Yify: ${requestErr || requestRes.statuscode}`) }

				const $ = cheerio.load (requestBody)

				if (!elementsByPage) {
					elementsByPage = $ ('.mv').length
				}


				$ ('.mv').each ((index, element) => {
					const link = $ (element).find ('a') ? $ (element).find ('a').attr ('href') || null : null;
					const seed = $ (element).find ('.mdif li:nth-child(6)') ? $ (element).find ('.mdif li:nth-child(6)').html () ? typeof $ (element).find ('.mdif li:nth-child(6)').html ().split ('</b>')[1] === 'number' ? $ (element).find ('.mdif li:nth-child(6)').html ().split ('</b>')[1] : -1 : -1 : -1;
					const leech = $ (element).find ('.mdif li:nth-child(7)') ? $ (element).find ('.mdif li:nth-child(7)').html () ? typeof $ (element).find ('.mdif li:nth-child(7)').html ().split ('</b>')[1] === 'number' ? $ (element).find ('.mdif li:nth-child(7)').html ().split ('</b>')[1] : -1 : -1 : -1;

					((options) => {
						request (url + link, (secondRequestErr, secondRequestRes, secondRequestBody) => {
							if (secondRequestErr) return

							const $$ = cheerio.load (secondRequestBody)
							let title = $$ ('.name h1')
							let quality = $$ ('.inattr > ul > li:nth-child(4)')
							let magnet = $$ ('.large.button.orange')
							let year = title && title.html () ? title.html ().match (/\([0-9]{4}\)/) : null

							title = title ? title.html () : null
							title = title ? title.split (/ \([0-9]{4}\)/) : null
							title = title ? entities.decode (title[0] || '') : null

							year = year ? year[0] : null
							year = year ? year.match (/[0-9]{4}/) : null
							year = year ? year[0] : null

							quality = quality ? quality.html () : null
							quality = quality ? quality.split (' ') : null
							quality = quality ? quality[1] : null

							magnet = magnet ? magnet.attr ('href') : null

							counter.j += 1;

							((query, movieInfos) => {
								request (`http://www.omdbapi.com/?t=${query || ''}&y=${movieInfos.year || ''}=&plot=short&r=json`, (imdbErr, imdbRes, imdbBody) => {
									counter.k += 1
									if (imdbErr || imdbRes.statusCode !== 200 || imdbBody[0] !== '{') { return }

									const movie = JSON.parse (imdbBody)

									if (movie && movie.Response === 'True') {
										let alreadyExists = false
										movieInfos.quality = movieInfos.quality ? movieInfos.quality.replace (/^[a-zA-Z]+/, (x) => {
											return x.toUpperCase ()
										}).replace (/^[0-9]+\w+/, (x) => {
											return x.toLowerCase ()
										}) : null
										for (let id = 0; id < movies.length; id += 1) {
											if (movies[id].title === movie.Title && movies[id].year === movie.Year) {
												alreadyExists = true
												let qualityExists = false
												for (let torrentsId = 0; torrentsId < movies[id].torrents.length; torrentsId += 1) {
													if (movies[id].torrents[torrentsId].quality === movieInfos.quality) {
														qualityExists = true
													}
												}
												if (!qualityExists) {
													movies[id].torrents.push ({
														quality: movieInfos.quality,
														seed: movieInfos.seed,
														leech: movieInfos.leech,
														magnet: movieInfos.magnet,
														isDownloading: false,
														percent: 0,
													})
												}
											}
										}
										if (!alreadyExists) {
											const newMovie = {
												title: movie.Title,
												year: movie.Year,
												runtime: movie.Runtime,
												genres: movie.Genre.split (/ ?, ?/),
												director: movie.Director.replace ('N/A', ''),
												actors: movie.Actors.split (/ ?, ?/),
												plot: movie.Plot,
												awards: movie.Awards,
												poster: movie.Poster,
												imdbId: movie.imdbID,
												rate: movie.imdbRating,
												parentalGuidance: movie.Rated.replace ('N/A', ''),

												torrents: [{
													quality: movieInfos.quality || 'HD',
													seed: movieInfos.seed,
													leech: movieInfos.leech,
													magnet: movieInfos.magnet,
													isDownloading: false,
													percent: 0,
												}],
											}
											movies.push (newMovie)
											Movie.findOrCreateUser (newMovie, (movie) => {})
										}
									}
									if (!timeouted && (counter.j === counter.k && counter.k >= elementsByPage * (opts.nbOfPages - 1))) {
										timeouted = true
										clearTimeout (timeOut)
										return cb (movies)
									}
									return true
								})
							}) (title, {
								magnet,
								quality,
								year,
								seed: options.seed,
								leech: options.leech,
							})
						})
					}) ({
						seed,
						leech,
					})
				})
			})
		}
	},
}
