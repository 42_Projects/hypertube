"use strict";

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const GitHubStrategy = require('passport-github2').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

const bcrypt = require('bcrypt');
const randomstring = require('randomstring');

passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	User.findOne({ id }, (err, user) => {
		done(err, user);
	});
});

passport.use(new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
	}, (username, password, done) => {
		User.findOne({$or: [{username : username}, {email : username}]}, (err, user) => {
			if (err) return done(err);
			if (!user) { return done(null, false, { message: 'Incorrect username.' }) }
			bcrypt.compare(password, user.password, (error, res) => {
				if (!res) return done(null, false, { message: 'Invalid Password' });
				const returnUser = {
					email: user.email,
					createdAt: user.createdAt,
					id: user.id,
					langue: user.langue,
				};
				return done(null, returnUser, { message: 'Logged In Successfully' });
			});
		});
	}
));

passport.use(new FacebookStrategy({
	clientID: '1496805380336287',
	clientSecret: 'fb5540ce6b7c0d8efeec504d391ad26e',
	callbackURL: 'http://localhost:1337/user/facebook/callback',
	profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified', 'photos'],
	enableProof: false,
	}, (accessToken, refreshToken, profile, done) => {
		User.findOne({ facebookId: profile.id }, (err, user) => {
			if (err) return done(err)
			if (!user) {
				const random = randomstring.generate();
				let email = '';
				if (!profile._json.email) {
					email = profile.id + '@facebook.com';
				} else {
					email = profile._json.email + '0';
				}
				User.create({
					facebookId: profile.id,
					username: profile._json.first_name +'_'+ profile.id,
					lastName: profile._json.last_name,
					firstName: profile._json.first_name,
					image: "https://graph.facebook.com/me/picture/?width=500&height=500&access_token=" + accessToken,
					langue: 'en',
					email,
					password: random,
          omniauth: true,
				}).exec((err, user) => {
					if (user) return done(null, user, { message: 'Logged In Successfully' })
					return done(err, null, { message: 'There was an error logging you in with Facebook' });
				});
			} else return done(null, user, { message: 'Logged In Successfully' })
		});
	}
));

passport.use(new GitHubStrategy({
	clientID: 'b06848ba5a336d2f5eef',
	clientSecret: '1fcdab57af567fd5489d254e643ce05c76f5b59d',
	callbackURL: 'http://localhost:1337/auth/github/callback',
	}, (accessToken, refreshToken, profile, done) => {
		User.findOne({ githubId: profile.id }, (err, user) => {
			if (err) return done(err)
			if (!user) {
				const random = randomstring.generate();
				User.create({
					githubId: profile.id,
					username: profile._json.login + '_'+ profile.id,
					lastName: profile._json.login,
					firstName: profile._json.login,
					email: profile.id + '@exemple.fr',
					image: profile._json.avatar_url,
					langue: 'en',
					password: random,
          omniauth: true,
				}).exec((err, user) => {
					if (user) return done(null, user, { message: 'Logged In Successfully' })
					return done(err, null, { message: 'There was an error logging you in with Facebook' });
				});
			} else return done(null, user, { message: 'Logged In Successfully' })
		});
	}
));

passport.use(new TwitterStrategy({
	consumerKey: '0PP2zMhNQbVugoEawxyprcoo7',
	consumerSecret: 'GfbkhZSXpR7sg2JhrQ1wXiEfDxrA7FJaCPhOAyKWTMtbkeAOQr',
	callbackURL: 'http://127.0.0.1:1337/auth/twitter/callback',
	includeEmail: true,
	}, (token, tokenSecret, profile, done) => {
		User.findOne({ twitterId: profile.id }, (err, user) => {
			if (err) return done(err)
			if (!user) {
				const random = randomstring.generate();
				console.log(profile._json.profile_image_url);

				let path = profile._json.profile_image_url.split('_normal');
				path[0] += ".jpg";
				console.log(path[0]);
				User.create({
					twitterId: profile.id + '_'+ profile.id,
					username: profile.username,
					lastName: profile.displayName,
					firstName: profile.displayName,
					email: profile.username + '@exemple.fr',
					image: path[0],
					langue: 'en',
					password: random,
          omniauth: true,
				}).exec((err, user) => {
					if (user) return done(null, user, { message: 'Logged In Successfully' })
					return done(err, null, { message: 'There was an error logging you in with Facebook' });
				});
			} else return done(null, user, { message: 'Logged In Successfully' })
		});
	}
));

passport.use(new GoogleStrategy({
	clientID: '403864183974-crq50srdknamc42illegluocev7i57j7.apps.googleusercontent.com',
	clientSecret: 'uNvEyM6V9Gp8d0ekJC5JE28F',
	callbackURL: 'http://localhost:1337/auth/google/callback',
	}, (token, tokenSecret, profile, done) => {
		User.findOne({ googleId: profile.id }, (err, user) => {
			if (err) return done(err)
			if (!user) {
				const random = randomstring.generate();
				let path = profile.photos[0].value.split('50');
				path[0] += "720";
				User.create({
					googleId: profile.id + '_' + profile.id,
					username: profile.name.givenName,
					lastName: profile.name.familyName,
					firstName: profile.name.givenName,
					email: profile.emails[0].value + '0',
					image: path[0],
					langue: 'en',
					password: random,
          omniauth: true,
				}).exec((err, user) => {
					if (user) return done(null, user, { message: 'Logged In Successfully' })
					return done(err, null, { message: 'There was an error logging you in with Facebook' });
				});
			} else return done(null, user, { message: 'Logged In Successfully' })
		});
	}
));
