const rmdir = require('rmdir');
const request = require ('request')

module.exports.cron = {
	myFirstJob: {
		schedule: '0 0 1 * * *',
		onTick: () => {
			console.log('Suppression des films non visionnés dans le mois'.cyan);
			request (`http://localhost:1337/admin/updateDb`, () => {
				request (`http://localhost:1337/api/v1/updateStats`, (err, response, body) => {
					Movie.find((err, movies) => {
						if (err) return console.log('erreur'.red);
						if (!movies) return console.log('erreur'.red);
						const date = new Date;
						// console.log(date);
						movies.forEach((movie) => {
							if ((movie.lastView !== '') && ((date - movie.lastView) > 2592000)) {
								console.log('Delete this film -> ' + movie.title);
								movie.torrents.forEach((resolution) => {
									if (resolution.path && resolution.path !== ''){
										rmdir('./movie/' + movie.id, (err, dirs, files) => {
											// console.log(dirs);
											// console.log(files);
											// console.log('all files are removed');
										});
										resolution.path = '';
										resolution.isDownloading = false;
										resolution.tmpPath = undefined;
										resolution.percent = undefined;
										movie.lastView = '';
										movie.save((err) => {
											if (err) return;
										})
									}
								});
							}
						});
					});
				})
			})
		}
	}
};
