/**
* Route Mappings
* (sails.config.routes)
*
* Your routes map URLs to views and controllers.
*
* If Sails receives a URL that doesn't match any of the routes below,
* it will check for matching files (images, scripts, stylesheets, etc.)
* in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
* might match an image file: `/assets/images/foo.jpg`
*
* Finally, if those don't match either, the default 404 handler is triggered.
* See `api/responses/notFound.js` to adjust your app's 404 logic.
*
* Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
* flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
* CoffeeScript for the front-end.
*
* For more information on configuring custom routes, check out:
* http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
*/

module.exports.routes = {

	/***************************************************************************
	*                                                                          *
	* Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
	* etc. depending on your default view engine) your home page.              *
	*                                                                          *
	* (Alternatively, remove this and add an `index.html` file in your         *
	* `assets` directory)                                                      *
	*                                                                          *
	***************************************************************************/

	// index
	'get /': 'IndexController.index',
	'post /': 'AuthController.login',

	// Partie Admin
	'get /admin': 'AdminController.admin',

	// Connexion Local
	'post /userCreate': 'UserController.create',
	// Step 2 register upload photo
	'/upload': 'UserController.upload',
	'post /userUpload': 'UserController.userUpload',

	// Connexion Facebook
	'/auth/facebook': 'AuthController.facebook',
	'/user/facebook/callback': 'AuthController.facebook/callback',

	// Connexion Github
	'/auth/github': 'AuthController.github',
	'/auth/github/callback': 'AuthController.github/callback',

	// Connexion Twitter
	'/auth/twitter': 'AuthController.twitter',
	'/auth/twitter/callback': 'AuthController.twitter/callback',

	// Connexion Google
	'/auth/google': 'AuthController.google',
	'/auth/google/callback': 'AuthController.google/callback',

	// // 42
	'/auth/42': 'UserController.auth',
	'/auth/42/callback': 'UserController.42/Callback',

	// Login - logout
	'/logout': 'AuthController.logout',

	// Page d'accueil apres connexion
	'/hypertube': 'HypertubeController.hypertube',

	// Page profile utilisateur
	'/myprofile': 'UserController.myprofile',

	// Page profile autre utilisateur
	'/profile/:id': 'UserController.profile',


	// ROUTE POUR LE STREAN ELLE PREND EN PARAMETTRE LE MAGNET DIRECTEMENT + Download
	'/stream/:id/:quality': 'StreamController.stream',
	'/watch/:id/:quality': 'StreamController.watch',
	'/download/:id/:quality': 'StreamController.download',

	// Commentaire
	'post /comment/:id': 'StreamController.comment',

	// // Supprimer son profil
	// '/delete': 'UserController.delete',

	// Reset password
	'post /reset': 'UserController.reset',

	// Change password
	'get /change-password/:token': 'UserController.changePassword',

	// newPawword
	'post /new-password/:token': 'UserController.newPassword',

	// Password Forgotten
	'/password-forgotten': 'UserController.passwordforgotten',

	/*
	Modif value in account
	*/

	'post /modify-password': 'ModifyController.modifyPassword',
	'post /modify-email': 'ModifyController.modifyEmail',
	'post /modify-username': 'ModifyController.modifyUsername',
	'post /modify-lastname': 'ModifyController.modifyLastName',
	'post /modify-firstname': 'ModifyController.modifyFirstName',

	/*
	* Route langue fr, es, en
	*/
	'/fr': 'LangueController.fr',
	'/es': 'LangueController.es',
	'/en': 'LangueController.en',
	'/it': 'LangueController.it',
	'/de': 'LangueController.de',

	'GET /api/v1/getMovies': {
		controller: 'APIController',
		action: 'getMovies',
	},

	'GET /api/v1/getActors': {
		controller: 'APIController',
		action: 'getActors',
	},

	'GET /api/v1/getDirectors': {
		controller: 'APIController',
		action: 'getDirectors',
	},

	'GET /api/v1/getGenres': {
		controller: 'APIController',
		action: 'getGenres',
	},

	'GET /api/v1/getQualities': {
		controller: 'APIController',
		action: 'getQualities',
	},

	'GET /api/v1/getParentalGuidances': {
		controller: 'APIController',
		action: 'getParentalGuidances',
	},

	'GET /api/v1/getHash': {
		controller: 'APIController',
		action: 'getHash',
	},

	'GET /api/v1/getAvailableSubtitles': {
		controller: 'APIController',
		action: 'getAvailableSubtitles',
	},

	'GET /api/v1/getSubtitles': {
		controller: 'APIController',
		action: 'getSubtitles',
	},

	'GET /api/v1/getTrailers': {
		controller: 'APIController',
		action: 'getTrailers',
	},

	'GET /api/v1/getStats': {
		controller: 'APIController',
		action: 'getStats',
	},

	'GET /api/v1/updateStats': {
		controller: 'APIController',
		action: 'updateStats',
	},

	'GET /admin/updateDb': {
		controller: 'APIController',
		action: 'updateDb',
	},

	'GET /admin/updateDbAll': {
		controller: 'APIController',
		action: 'updateDbAll',
	},

	'GET /admin/clearMovies': {
		controller: 'APIController',
		action: 'clearMovies',
	},

	'GET /youtube': {
		view: 'youtube',
	},

	/***************************************************************************
	*                                                                          *
	* Custom routes here...                                                    *
	*                                                                          *
	* If a request to a URL doesn't match any of the custom routes above, it   *
	* is matched against Sails route blueprints. See `config/blueprints.js`    *
	* for configuration options and examples.                                  *
	*                                                                          *
	***************************************************************************/

};
