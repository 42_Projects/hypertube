# Hypertube

A streaming website. It download movies on the server from torrents, then stream them to the client over browser.

This project works with Sails.js. So you need to install it before running the server.

To run server
```shell
npm install
sails lift
```

Database get updated each day at 01:00AM. Il you want tu update it manually, go to /admin (hypertube:hypertube).

![Alt text](https://gitlab.com/42_Projects/hypertube/raw/master/.h1.png)
![Alt text](https://gitlab.com/42_Projects/hypertube/raw/master/.h2.png)
