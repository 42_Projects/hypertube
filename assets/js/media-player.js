// Wait for the DOM
document.addEventListener("DOMContentLoaded", function() { initialiseMediaPlayer(); }, false);

var mediaPlayer;
var progressBar;

function initialiseMediaPlayer() {
	// Get a handle to the player
	mediaPlayer = document.getElementById('video');
	mediaPlayer.style.display = 'none';

	// Get handles to each of the buttons and required elements
	progressBar = document.getElementById('progress-bar');

	mediaPlayer.addEventListener('timeupdate', updateProgressBar, false);
	mediaPlayer.addEventListener('canplay', canplayLog, false);
	mediaPlayer.addEventListener('waiting', waitingLog, false);
	mediaPlayer.addEventListener('ended', function() { this.pause(); }, false);
}

function canplayLog() {
	// console.log('canplayLog()')
	mediaPlayer.style.display = 'block';
	setTimeout(function() {
		mediaPlayer.play();
	}, 1000)
	if (document.getElementById('playerYT')) { document.getElementById('playerYT').remove() }
}

function waitingLog() {
	// console.log('waitingLog()')
}

// Replays the media currently loaded in the player
function replayMedia() {
	resetPlayer();
	mediaPlayer.play();
}

// Update the progress bar
function updateProgressBar() {
	// Work out how much of the media has played via the duration and currentTime parameters
	var percentage = Math.floor((100 / mediaPlayer.duration) * mediaPlayer.currentTime);
	// Update the progress bar's value
	progressBar.value = percentage;
	// Update the progress bar's text (for browsers that don't support the progress element)
	progressBar.innerHTML = percentage + '% played';
}

// Updates a button's title, innerHTML and CSS class to a certain value
function changeButtonType(btn, value) {
	btn.title = value;
	btn.innerHTML = value;
	btn.className = value;
}

// Loads a video item into the media player
function loadVideo() {
	for (var i = 0; i < arguments.length; i++) {
		var file = arguments[i].split('.');
		var ext = file[file.length - 1];
		// Check if this media can be played
		if (canPlayVideo(ext)) {
			// Reset the player, change the source file and load it
			resetPlayer();
			mediaPlayer.src = arguments[i];
			mediaPlayer.load();
			break;
		}
	}
}

// Checks if the browser can play this particular type of file or not
function canPlayVideo(ext) {
	var ableToPlay = mediaPlayer.canPlayType('video/' + ext);
	if (ableToPlay == '') return false;
	else return true;
}

function toggleFullScreen(){
	if(mediaPlayer.requestFullScreen){
		mediaPlayer.requestFullScreen();
	} else if(mediaPlayer.webkitRequestFullScreen){
		mediaPlayer.webkitRequestFullScreen();
	} else if(mediaPlayer.mozRequestFullScreen){
		mediaPlayer.mozRequestFullScreen();
	}
}
