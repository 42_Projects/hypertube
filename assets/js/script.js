$(function() {

    /////////////////////////////////////
    // Upload profile
    /////////////////////////////////////
    function loadProfileImage() {
        $('#loadProfileImage').attr('src', $('#loadProfileImage').data('src'))
    }
    setTimeout(loadProfileImage, 2500);

    /////////////////////////////////////
    // Login
    /////////////////////////////////////
    $('#formLogin').submit(function(e) {

        e.preventDefault();

        var $form = $(this);
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {
            $('#alertLogin').show();
            $('#alertLogin').text(data.message);
        });

        posting.done(function(data) {

          if (typeof data == 'string') {
              window.location.replace('/hypertube');
          } else {
              $('#alertLogin').show();
              $('#alertLogin').text(data.message);
          }

        });

    });

    $('#formRegister').submit(function(e) {
        e.preventDefault();

        var $form = $(this);
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {
            $('#alertRegister').show();
            $('#alertRegister').text(data.message);
        });
        posting.done(function(data) {
            if (typeof data == 'object') {
                if (data.message !== undefined)
                {
                    if (typeof data.message == 'string')
                    {
                        $('#alertRegister').show();
                        $('#alertRegister').text(data.message);
                    }
                    if (typeof data.message == 'object')
                    {
                        var message;
                        if (data.message.invalidAttributes.email !== undefined) message = data.message.invalidAttributes.email[0].message;
                        if (data.message.invalidAttributes.username !== undefined) message = data.message.invalidAttributes.username[0].message;
                        if (data.message.invalidAttributes.lastname !== undefined) message = data.message.invalidAttributes.lastname[0].message;
                        if (data.message.invalidAttributes.firstname !== undefined) message = data.message.invalidAttributes.firstname[0].message;
                        $('#alertRegister').show();
                        $('#alertRegister').text(message);
                    }
                }
            }
            else
            {
                if ($('#inputPasswordRegister').val().length >= 6) {
                    var str = $('#inputPasswordRegister').val();
                    var patt = /(?=.*[0-9])/;
                    var result = patt.test(str);
                    if (result == false) {
                        $('#formGroupPasswordRegister').addClass('has-error');
                        $('#inputPasswordRegister').next().text('Your password is too weak');
                        $('#inputPasswordRegister').next().show();
                    } else {
                        $('#formGroupPasswordRegister').removeClass('has-error');
                        $('#inputPasswordRegister').next().text('');
                        $('#inputPasswordRegister').next().hide();
                        window.location.replace('/upload');
                    }
                }
            }
        });
    });

    /////////////////////////////////////
    // Update profile
    /////////////////////////////////////

    // Email
    $('#formEmailUpdate').hide();
    $('#toggleEmailUpdate').click(function () {
        $('#formEmailUpdate').toggle();
    });
    $('#submitEmailUpdate').click(function (e) {
        e.preventDefault();

        var $form = $('#formEmailUpdate');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alertUpdate').show();
            if (data.error) {
                $('#alertUpdate').removeClass('alert-success');
                $('#alertUpdate').addClass('alert-danger');
                $('#alertUpdate').text(data.error);
            }
            if (data.success) {
                $('#alertUpdate').removeClass('alert-danger');
                $('#alertUpdate').addClass('alert-success');
                $('#alertUpdate').text(data.success);
            }
        });

    });

    // Username
    $('#formUsernameUpdate').hide();
    $('#toggleUsernameUpdate').click(function () {
        $('#formUsernameUpdate').toggle();
    });
    $('#submitUsernameUpdate').click(function (e) {
        e.preventDefault();

        var $form = $('#formUsernameUpdate');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alertUpdate').show();
            if (data.error) {
                $('#alertUpdate').removeClass('alert-success');
                $('#alertUpdate').addClass('alert-danger');
                $('#alertUpdate').text(data.error);
            }
            if (data.success) {
                $('#alertUpdate').removeClass('alert-danger');
                $('#alertUpdate').addClass('alert-success');
                $('#alertUpdate').text(data.success);
            }
        });
    });

    // Lastname
    $('#formLastNameUpdate').hide();
    $('#toggleLastnameUpdate').click(function () {
        $('#formLastNameUpdate').toggle();
    });
    $('#submitLastNameUpdate').click(function (e) {
        e.preventDefault();

        var $form = $('#formLastNameUpdate');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alertUpdate').show();
            if (data.error) {
                $('#alertUpdate').removeClass('alert-success');
                $('#alertUpdate').addClass('alert-danger');
                $('#alertUpdate').text(data.error);
            }
            if (data.success) {
                $('#alertUpdate').removeClass('alert-danger');
                $('#alertUpdate').addClass('alert-success');
                $('#alertUpdate').text(data.success);
            }
        });
    });

    // Firstname
    $('#formFirstNameUpdate').hide();
    $('#toggleFirstnameUpdate').click(function () {
        $('#formFirstNameUpdate').toggle();
    });
    $('#submitFirstNameUpdate').click(function (e) {
        e.preventDefault();

        var $form = $('#formFirstNameUpdate');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alertUpdate').show();
            if (data.error) {
                $('#alertUpdate').removeClass('alert-success');
                $('#alertUpdate').addClass('alert-danger');
                $('#alertUpdate').text(data.error);
            }
            if (data.success) {
                $('#alertUpdate').removeClass('alert-danger');
                $('#alertUpdate').addClass('alert-success');
                $('#alertUpdate').text(data.success);
            }
        });
    });

    // Password
    $('#formPasswordUpdate').hide();
    $('#togglePasswordnameUpdate').click(function () {
        $('#formPasswordUpdate').toggle();
    });
    $('#submitPasswordUpdate').click(function (e) {
        e.preventDefault();

        var $form = $('#formPasswordUpdate');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alertUpdate').show();
            if (data.error) {
                $('#alertUpdate').removeClass('alert-success');
                $('#alertUpdate').addClass('alert-danger');
                $('#alertUpdate').text(data.error);
            }
            if (data.success) {
                $('#alertUpdate').removeClass('alert-danger');
                $('#alertUpdate').addClass('alert-success');
                $('#alertUpdate').text(data.success);
            }
        });
    });

    /////////////////////////////////////
    // Password forgotten request page
    /////////////////////////////////////
    $('#formPasswordForgotten').submit(function (e) {
        e.preventDefault();

        var $form = $('#formPasswordForgotten');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alert').show();

            if (data.error) {
                $('#alert').removeClass('alert-success');
                $('#alert').addClass('alert-danger');
                $('#alert').text(data.error);
            }

            if (data.success) {
                $('#alert').removeClass('alert-danger');
                $('#alert').addClass('alert-success');
                $('#alert').text(data.success);
            }
        });
    });

    /////////////////////////////////////
    // Update password
    /////////////////////////////////////
    $('#formNewPassword').submit(function (e) {
        e.preventDefault();

        var $form = $('#formNewPassword');
        var url = $form.attr("action");

        var posting = $.post(url, $form.serialize());
        posting.fail(function(data) {

        });

        posting.done(function(data) {
            $('#alert').show();

            if (data.error) {
                $('#alert').removeClass('alert-success');
                $('#alert').addClass('alert-danger');
                $('#alert').text(data.error);
            }

            if (data.success) {
                $('#alert').removeClass('alert-danger');
                $('#alert').addClass('alert-success');
                $('#alert').text(data.success);
            }
        });
    });

});
