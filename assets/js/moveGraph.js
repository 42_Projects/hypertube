var b1 = document.getElementById("b1");
var b2 = document.getElementById("b2");
var b3 = document.getElementById("b3");
var b4 = document.getElementById("b4");

var one = document.getElementById("one");
var two = document.getElementById("two");
var three = document.getElementById("three");
var four = document.getElementById("four");

var desc = document.getElementById("description");
var title = document.getElementById("title");

b1.addEventListener('click', () => {
  del_button();
  del_canvas();
  desc.innerHTML = 'Classement des films par rapport a leur note IMDB';
  title.innerHTML = 'Note IMDB';
  b1.className = 'valid';
  one.style.display = 'block';
});

b2.addEventListener('click', () => {
  del_button();
  del_canvas();
  desc.innerHTML = 'Nombre d\'utilisateurs total inscrit, connecté, trié par langue et connexion omniauth';
  title.innerHTML = 'Graphique utilisateurs';
  b2.className = 'valid';
  two.style.display = 'block';
});

b3.addEventListener('click', () => {
  del_button();
  del_canvas();
  desc.innerHTML = 'Classement des films par année de production';
  title.innerHTML = 'Année de production';
  b3.className = 'valid';
  three.style.display = 'block';
});

b4.addEventListener('click', () => {
  del_button();
  del_canvas();
  desc.innerHTML = 'Action sur la database';
  title.innerHTML = 'DataBase';
  b4.className = 'valid';
  four.style.display = 'block';
});

const del_button = () => {
  b1.className = '';
  b2.className = '';
  b3.className = '';
  b4.className = '';
};

const del_canvas = () => {
  one.style.display = 'none';
  two.style.display = 'none';
  three.style.display = 'none';
  four.style.display = 'none';
  document.getElementById('message').style.display = 'none';
};

$('#b5').click((e) => {
  e.preventDefault();
  $.get('/admin/clearMovies' , (data) => {
    $('#message').text(data);
    $('#message').show();
  });
});

$('#b6').click((e) => {
  e.preventDefault();
  $.get('/admin/updateDb' , (data) => {
    $('#message').text(data);
    $('#message').show();
  });
});

$('#b7').click((e) => {
  e.preventDefault();
  $.get('/admin/updateDbAll' , (data) => {
    $('#message').text(data);
    $('#message').show();
  });
});

